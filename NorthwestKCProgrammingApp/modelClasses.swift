//
//  modelClasses.swift
//  NorthwestKCProgrammingApp
//
//  Created by Bourishetty,Karun on 3/12/19.
//  Copyright © 2019 Bourishetty,Karun. All rights reserved.
//

import Foundation

// This extension is necessary for our Notifications mechanism
extension Notification.Name {
    static let SchoolsRetrieved = Notification.Name("Schools Retrieved")
    static let TeamsForSelectedSchoolRetrieved = Notification.Name("Teams for Selected School Retrieved")
    static let TeamsRetrieved = Notification.Name("Teams Retrieved")
}

@objcMembers
class Schools: NSObject {

    private var schools:[School]
    
    private override init(){
        self.schools = []
    }
    
    func numSchools()->Int{
        return schools.count
    }
    
    subscript(school:Int)->School{
        return schools[school]
    }
    
    func add(school:School){
        schools.append(school)
    }
    
    func delete(school:School){
        for i in 0 ..< schools.count {
            if school.name == schools[i].name{
                schools.remove(at: i)
                break
            }
        }
    }
    
    static var shared = Schools()
}

@objcMembers
class School:NSObject{
    var name:String
    var coach:String
    var teams:[Team] = []
    
    init(name:String,coach:String){
        self.name = name
        self.coach = coach
        self.teams=[]
    }
    
//    func add(name:String,coach:String){
//        self.name = name
//        self.coach = coach
//    }
    
    subscript(team:Int)->Team{
        return teams[team]
    }
    
    func addTeam(name:String, students:[String]){
        teams.append(Team.init(names: name, students: students))
    }
    
}


class Team{
    var teamName:String
    var teamStudents:[String]
    
    let backendless = Backendless.sharedInstance()!
    var cityDataStore:IDataStore!
    var touristSitesDataStore:IDataStore!
    static var shared:Team = Team() // our singleton
    
    var cities:[City] =  []              // all cities in our model
    var touristSites:[TouristSite] = []  // all tourist sites. Currently unused ...
    
    subscript(index:Int) -> City {      // now we can write sharedInstance[j] to retrieve the jth City
        return cities[index]
    }
    
    var selectedCity:City?                              // the city that we selected in CitiesTableViewController and will display tourist sites for
    var touristSitesForSelectedCity:[TouristSite] = [] // tourist sites for our selected city
    
    
     init(names:String,students:[String]){
        self.teamName = names
        self.teamStudents = students
    }

// fetch all schools and their teams asychronously, storing results in cities
func retrieveAllSchoolsAsynchronously() {
    let queryBuilder = DataQueryBuilder()
    queryBuilder!.setRelated(["touristSites"]) // TouristSites referenced in City's touristSites                                             // field will be retrieved for each City
    queryBuilder!.setPageSize(100) // up to 100 TouristSites can be retrieved for each City
    cityDataStore.find(queryBuilder, response: {(results) -> Void in
        self.cities = results as! [City]
        NotificationCenter.default.post(name: .SchoolsRetrieved, object: nil) // broadcast a Notification that Cities have been retrieved
    }, error: {(exception) -> Void in
        print(exception.debugDescription)
    })
    
}

// fetch all teams, storing results in touristSites
func retrieveAllTeamsAsynchronously() {
    let startDate = Date()
    
    touristSitesDataStore.find({
        (retrievedTouristSites) -> Void in
        self.touristSites = retrievedTouristSites as! [TouristSite]
        NotificationCenter.default.post(name: .TeamsRetrieved, object: nil) // broadcast a Notification that tourist sites have been retrieved
        
    },
                               error: {(exception) -> Void in
                                print(exception.debugDescription)
    })
    print("Done in \(Date().timeIntervalSince(startDate)) seconds ")
}

// fetches Teams for selected School and reloads touristSitesForSelectedCity
func retrieveTeamForSelectedSchoolAsynchronously() {
    let startDate = Date()
    
    let queryBuilder:DataQueryBuilder = DataQueryBuilder()
    queryBuilder.setWhereClause("name = '\(self.selectedCity?.name! ?? "")'" ) // restrict ourselves to one city
    queryBuilder.setPageSize(100)
    queryBuilder.setRelated( ["touristSites"] )
    self.cityDataStore.find(queryBuilder,
                            response: {(results) -> Void in
                                let city = results![0] as! City
                                self.touristSitesForSelectedCity = city.touristSites
                                NotificationCenter.default.post(name: .TeamsForSelectedSchoolRetrieved, object: nil) // broadcast the fact that tourist sites for selected city have been retrieved
    }, error: {(exception) -> Void in
        print(exception.debugDescription)
    })
    print("Done in \(Date().timeIntervalSince(startDate)) seconds ")
}

// creates a City, saves it both locally and in the data store
func saveSchoolAsynchronously(named name:String, population:Int) {
    
    //
    
    var cityToSave = City(name: name, population: population, touristSites: [])
    cityDataStore.save(cityToSave, response: {(result) -> Void in
        cityToSave = result as! City
        self.cities.append(cityToSave)
        self.retrieveAllCitiesAsynchronously()},
                       error:{(exception) -> Void in
                        print(exception.debugDescription)
                        
    })
    
    //
}

// saves a (new) tourist site for the selected city
func saveTeamForSelectedSchool(team:Team) {
    print("Saving the tourist site for the selected city...")
    let startingDate = Date()
    Types.tryblock({
        let savedTouristSite = self.touristSitesDataStore.save(team) as! Team // save one of its Team
        self.cityDataStore.addRelation("touristSites:TouristSite:n", parentObjectId: self.selectedCity!.objectId, childObjects: [savedTouristSite.objectId!])
        
    }, catchblock:{ (exception) -> Void in
        print(exception.debugDescription)
    })
    
    print("Done!! in \(Date().timeIntervalSince(startingDate)) seconds")
}
}
